package tasks;

import java.util.Scanner;

/**
 * .       Запросить у пользователя число. Посчитать и вывести :
 * - количество разрядов этого числа
 * - сумму разрядов этого числа
 * - наибольшую цифру в разрядах
 * - число, в котором цифры разрядов будут зеркально переставлены
 */
public class TaskTree {
    private static Scanner s = new Scanner(System.in);
    private int number;

   public TaskTree() {
        System.out.println("Введите ваше число: ");
        number = s.nextInt();
        getDigitNumber(number);

    }

    public static void getDigitNumber(Integer d) {
        String userNumber = d.toString();
        System.out.println("Количество разрядов числа " + userNumber.length());
        char[] number = new char[userNumber.length()];
        userNumber.getChars(0, userNumber.length(), number, 0);
        int[] arrNumber = new int[number.length];

        for (int i = 0; i < number.length; i++) {
            arrNumber[i] = Character.getNumericValue(number[i]);
        }
        int max = 0;
        int sum = 0;
        for (int i = 0; i < arrNumber.length; i++) {
            sum += arrNumber[i];
            if (arrNumber[i] > max) {
                max = arrNumber[i];
            }
        }

        System.out.println("Сумма величин разрядов равна : " + sum);
        System.out.println("Наибольшая величина разряда : " + max);
        System.out.print("Зеркальное число : ");
        for (int i = arrNumber.length -1; i >= 0;i--){
            System.out.print(arrNumber[i]);
        }
    }
}
