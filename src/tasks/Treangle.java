package tasks;

import java.util.Scanner;

/**
 * .       Нарисуйте в консоли равнобедренный треугольник.
 * Высоту запросите у пользователя.
 * Пользователь также должен выбрать полый или сплошной вариант
 */
public class Treangle {
    static Scanner s = new Scanner(System.in);
    private int heigth;

    public int getHeigth() {
        return heigth;
    }

    public Treangle() {
        System.out.println("Какой треугольник вы хотите нарисовать :\n 1 - Равнобедренный \n 2 - Прямоугольный ");
        int userSelect = s.nextInt();
        System.out.println("Каким он будет  :\n 1 - полым \n 2 - сплошным");
        int tmp = s.nextInt();
        System.out.println("Укажите высоту треугольника :");
        heigth = s.nextInt();
        if (userSelect == 1) {
            switch (tmp) {
                case 1:
                    drawMeHollowTreangle();
                    break;
                case 2:
                    drawMeFullTreangle(heigth);
                    break;

            }
        }

        if (userSelect == 2)
            switch (tmp) {
                case 1:
                    drawMeHollowRectagularTreangle();
                    break;
                case 2:
                    drawMeFullRectangularTreangle();
                    break;

            }
    }

    public Treangle(int base) {
        System.out.println(" Из скольки уровней вы хотите создать елочку?");
        int level = s.nextInt();
        int tmpLevel = level;
        int i = 0;
        if (i >= 0) {
            while (i != level) {
                drawMeFir(base - level, tmpLevel);
                if (base >= 1) {
                    base++;
                }
                i++;
                tmpLevel--;
            }

        } else
            System.out.println("Вы ввели неправильное число! Нужно было ввести  целое положительное число");
    }

    public void drawMeHollowTreangle() {
        char[][] treangle = new char[heigth][heigth * 2];
        int middle = treangle.length;
        for (int i = 0; i < treangle.length; i++) {
            for (int j = 0; j < treangle[i].length; j++) {
                treangle[i][j] = ' ';
                if (i == 0 && j == middle) {
                    treangle[i][j] = '#';
                }
                if (i != 0 && (j == middle - i) || j == middle + i) {
                    treangle[i][j] = '#';
                }
                if (i == treangle.length - 1 && j != 0) {
                    treangle[i][j] = '#';
                }
                System.out.print(treangle[i][j]);
            }
            System.out.println();
        }
    }

    public void drawMeFullTreangle(int heigth) {

        char[][] treangle = new char[heigth][heigth * 2];
        int middle = treangle.length;
        for (int i = 0; i < treangle.length; i++) {
            for (int j = 0; j < treangle[i].length; j++) {
                treangle[i][j] = ' ';
                if (i == 0 && j == middle) {
                    treangle[i][j] = '#';
                }
                if (i != 0 && (j >= middle - i) && j <= middle + i) {
                    treangle[i][j] = '#';
                }
                if (i == treangle.length - 1 && j != 0) {
                    treangle[i][j] = '#';
                }
                System.out.print(treangle[i][j]);
            }
            System.out.println();
        }
    }

    public void drawMeHollowRectagularTreangle() {
        char[][] treangle = new char[heigth][heigth * 2];
        for (int i = 0; i < treangle.length; i++) {
            for (int j = 0; j < treangle[i].length; j++) {
                treangle[i][j] = ' ';
                if (i == 0 && j == 0) {
                    treangle[i][j] = '#';
                }
                if (i != 0 && (j == 0) || j == i) {
                    treangle[i][j] = '#';
                }
                if (i == treangle.length - 1 && (j >= 0 && j <= i)) {
                    treangle[i][j] = '#';
                }
                System.out.print(treangle[i][j]);
            }
            System.out.println();
        }
    }

    public void drawMeFullRectangularTreangle() {

        char[][] treangle = new char[heigth][heigth * 2];
        for (int i = 0; i < treangle.length; i++) {
            for (int j = 0; j < treangle[i].length; j++) {
                treangle[i][j] = ' ';
                if (i == 0 && j == 0) {
                    treangle[i][j] = '#';
                }
                if (i != 0 && (j >= 0 && j <= i)) {
                    treangle[i][j] = '#';
                }
                if (i == treangle.length - 1 && (j >= 0 && j <= i)) {
                    treangle[i][j] = '#';
                }
                System.out.print(treangle[i][j]);
            }
            System.out.println();
        }
    }

    public void drawMeFir(int heigth, int level) {

        StringBuilder stringBuilder = new StringBuilder();
        char[][] treangle = new char[heigth][heigth * 2];
        int middle = treangle.length;
        for (int i = 0; i < treangle.length; i++) {
            int lev = level;
            for (int j = 0; j < treangle[i].length; j++) {
                treangle[i][j] = ' ';
                if (i == 0 && j == middle) {
                    treangle[i][j] = '#';
                }
                if (i != 0 && (j >= middle - i) && j <= middle + i) {
                    treangle[i][j] = '#';
                }
                if (i == treangle.length - 1 && j != 0) {
                    treangle[i][j] = '#';
                }


            }

        }
        int numberOfToys = (int) (Math.random() * heigth);
        while (numberOfToys > 0) {
            int a = (int) (Math.random() * heigth);
            int b = (int) (Math.random() * (heigth * 2));
            if (treangle[a][b] == '#') {
                treangle[a][b] = '@';
                numberOfToys--;
            }
        }

        for (int i = 0; i < treangle.length; i++) {
            int lev = level;
            for (int j = 0; j < treangle[i].length; j++) {

                while (lev != 0) {
                    stringBuilder.append(" ");
                    lev--;
                }

                stringBuilder.append(treangle[i][j]);
            }
            lev = level;
            stringBuilder.append("\n");
        }
        System.out.print(stringBuilder);
    }

    public static void firTree() {
        System.out.println("Введите размер треугольника в основании елочки : ");
        int base = s.nextInt();
        Treangle treangle = new Treangle(base);
    }
}





