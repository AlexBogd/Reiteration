package tasks;

import java.util.Scanner;

/**
 Повторите функционал задачи 1, однако прямоугольник должен быть полый.
 */
public class TaskTwo {

    public static void showEmptyRectangle(){
        Scanner sc = new Scanner(System.in);
        StringBuilder sb = new StringBuilder(" Эта программа рисует пустой прямоугольник в консоли \n");
        sb.append("Но сперва необходимо указать длинны сторон в символах \n");
        sb.append("Сторона а равна :");
        System.out.println(sb);
        int a = sc.nextInt();
        System.out.println("Сторона b равна :");
        int b = sc.nextInt();
        char[][] rectangle = new char[a][b];
        for (int i = 0; i < rectangle.length; i++){
            for (int j = 0; j< rectangle[i].length;j++){
                if(i ==0 || i == (rectangle.length-1)){
                rectangle[i][j] = '#';
                System.out.print(rectangle[i][j]);
                }

                else {
                    rectangle[i][j] = ' ';
                    rectangle[i][0]='#';
                    rectangle[i][rectangle[i].length-1] ='#';
                    System.out.print(rectangle[i][j]);
                }

            }
            System.out.println();
        }

    }

    }

