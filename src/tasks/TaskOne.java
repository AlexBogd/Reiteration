package tasks;
import java.util.Scanner;
import java.lang.StringBuilder;
/**
 Нарисуйте в консоли прямоугольник. Запросите у пользователя размер сторон в символах.
 Для вывода используйте любой спец. символ - @, #,$ и т.д.
 */
public class TaskOne {


    public static void show() {
        Scanner sc = new Scanner(System.in);
        StringBuilder sb = new StringBuilder(" Эта программа рисует прямоугольник в консоли \n");
        sb.append("Но сперва необходимо указать длинны сторон в символах \n");
        sb.append("Сторона а равна :");
        System.out.println(sb);
        int a = sc.nextInt();
        System.out.println("Сторона b равна :");
        int b = sc.nextInt();
        char[][] rectangle = new char[a][b];
        for (int i = 0; i < rectangle.length; i++){
            for (int j = 0; j< rectangle[i].length;j++){
                rectangle[i][j] = '#';
                System.out.print(rectangle[i][j]);
            }
            System.out.println();
        }

    }

}
