package tasks;

import java.util.Scanner;

/**
 * Создать метод, принимающий на вход число
 * и возвращающий булевское значение – простое оно или нет.
 */
public class Numerals {
    static private Scanner s = new Scanner(System.in);

    public Numerals() {
        StringBuilder stringBuilder = new StringBuilder("Какое действие вы хотите совершить? \n");
        stringBuilder.append("\t 1. Вывести сумму четных и нечетных разрядов заданного числа. \n");
        stringBuilder.append("\t 2. Напечатать количество цифв в заданном числе \n");
        stringBuilder.append("\t 3. Напечатать числа от нуля до заданного \n");
        stringBuilder.append("\t 4. Напечатать среднее значение трех заданных чисел \n");
        stringBuilder.append("\t 5. Напечатать среднее значение заданных чисел \n");
        stringBuilder.append("\t 6. Напечатать большее из двух чисел \n");
        stringBuilder.append("\t 7. Напечатать остаток от деления двух чисел \n");
        stringBuilder.append("\t 8. Вычислить равны ли два заданных числа \n");
        stringBuilder.append("\t 9. Вычислить факториал заданного числа \n");
        stringBuilder.append("\t 10. Узнать простое число или нет \n");
        stringBuilder.append("\t 11. Разложить число на множители \n");
        stringBuilder.append("\t 12. Вычислить наименьшее общее кратное \n");
        System.out.println(stringBuilder);
        int n = s.nextInt();
        switch (n) {
            case 1:
                sumOfDigits();
                break;
            case 2:
                System.out.println("Введите число :");
                int num = s.nextInt();
                searchNumberOfDigits(num);
                break;
            case 3:
                System.out.println("Введите число :");
                int numTo = s.nextInt();
                fromZeroTo(numTo);
                break;
            case 4:
                System.out.println("Введите первое число :");
                int numOne = s.nextInt();
                System.out.println("Введите второе число :");
                int numTwo = s.nextInt();
                System.out.println("Введите третье число :");
                int numThree = s.nextInt();
                averageOfThreeNumbers(numOne, numTwo, numThree);
                break;
            case 5:
                System.out.println("Среднее значение скольки цифр вы хотите посчитать");
                int numberOfDigits = s.nextInt();
                Integer[] arrayOfDigits = new Integer[numberOfDigits];
                for (int i = 0; i <= arrayOfDigits.length - 1; i++) {
                    System.out.println("Введите " + (i + 1) + "е число");
                    arrayOfDigits[i] = s.nextInt();
                }
                System.out.println("Среднее значение введенных цифр :" + averageOfNumbers(arrayOfDigits));
                break;
            case 6:
                System.out.println("Введите первое число");
                double firstNumber = s.nextDouble();
                System.out.println("Введите второе число");
                double secondNumber = s.nextDouble();
                System.out.println("Большее число : " + returnLargerNumber(firstNumber, secondNumber));
                break;
            case 7:
                System.out.println("Введите первое число");
                firstNumber = s.nextDouble();
                System.out.println("Введите второе число");
                secondNumber = s.nextDouble();
                System.out.println("Остаток от деления первого числа на второе : " + remainOfdevision(firstNumber, secondNumber));
                break;
            case 8:
                System.out.println("Введите первое число");
                firstNumber = s.nextDouble();
                System.out.println("Введите второе число");
                secondNumber = s.nextDouble();
                boolean equivalent = equivalentNumber(firstNumber, secondNumber);
                if (equivalent) {
                    System.out.println("Числа равны! ");
                } else System.out.println("Числа не равны! ");

                break;
            case 9:
                System.out.println("Введите целое положительное число:");
                num = s.nextInt();
                System.out.println("Факториал вычисленный с помощью цикла равен :" + cycleFact(num));
                System.out.println("Факториал вычисленный с помощью рекурсии равен :" + recursionFact(num));
            case 10:
                System.out.println("Введите число : ");
                num = s.nextInt();
                if (simpleNumber(num)) {
                    System.out.println("Число простое");
                } else System.out.println("Число не является простым");
            case 11:
                System.out.println("Введите число : ");
                num = s.nextInt();
                factorization(num);
            case 12:
                System.out.println("Введите первое число");
                int numberOne = s.nextInt();

                System.out.println("Введите второе число");
                int numberTwo = s.nextInt();
                System.out.println(nok(numberOne,numberTwo));
        }
    }

    private static void sumOfDigits() {
        System.out.println("Введите число которое содержит не менее четырех разрядов : ");
        double number = s.nextDouble();
        while (number / 1000 < 1) {
            System.out.println(" Вы ввели неверное число, повторите попытку!");
            number = s.nextDouble();
        }
        int par = 1;
        int sumEven = 0;
        int sumUneven = 0;
        while (number > 1) {
            if (par % 2 == 0) {
                sumEven += number % 10;
            }
            if (par % 2 != 0) {
                sumUneven += number % 10;
            }
            number = number / 10;
            par++;
        }
        System.out.println("Сумма цифр нечетных разрядов равна : " + sumUneven);
        System.out.println("Сумма цифр четных разрядов равна : " + sumEven);
    }

    private static void searchNumberOfDigits(int number) {
        Integer numberC = number;
        String numberOfDigit = numberC.toString();
        numberOfDigit = numberOfDigit.trim();
        System.out.println("Количество цифр в числе равно " + numberOfDigit.length());
    }

    private static void fromZeroTo(int number) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (number >= 0) {
            sb.append(i);
            sb.append("\t");
            i++;
            if (i % 10 == 0) {
                sb.append("\n");
            }
            number--;
        }
        System.out.println(sb);
    }

    private static void averageOfThreeNumbers(double one, double two, double three) {
        double averageSum = (one + two + three) / 3;
        System.out.println("Среднее значение равно :" + averageSum);
    }

    private static double averageOfNumbers(Integer... numbers) {
        double averageSum = 0;
        for (int sum : numbers) {
            averageSum += sum;
        }
        averageSum = averageSum / numbers.length;
        return averageSum;
    }

    private static double returnLargerNumber(double firstNumber, double secondNumber) {
        if (firstNumber / secondNumber > 1) {
            return firstNumber;
        }
        if (secondNumber / firstNumber > 1) {
            return secondNumber;
        } else return 0;
    }

    private static double remainOfdevision(double firstNumber, double secondNumber) {

        return firstNumber % secondNumber;
    }

    private static boolean equivalentNumber(double firstNumber, double secondNumber) {
        if (firstNumber == secondNumber) {
            return true;
        } else {
            return false;
        }
    }

    private static int cycleFact(int number) {
        int factorial = 1;
        while (number > 0) {
            factorial *= number;
            number--;
        }
        return factorial;
    }

    private static int recursionFact(int number) {
        int factorial;
        if (number == 1) {
            return 1;
        }
        factorial = recursionFact(number - 1) * number;
        return factorial;
    }

    private static boolean simpleNumber(int number) {
        if (number % 2 == 0 && number != 2) {
            return false;
        }
        int numbersqrt = (int) Math.sqrt(number);
        while (numbersqrt > 1) {
            if (number % numbersqrt == 0 && number != numbersqrt) {
                return false;
            }
            numbersqrt--;
        }
        return true;
    }

    private static void factorization(int number) {
        StringBuilder sb = new StringBuilder("Простые делители вашего числа :");
        if (simpleNumber(number)) {
            System.out.println("Число простое, его делители 1 и " + number);
        } else {
            int devider = 2;
            while (number > 1) {
                while (number % devider == 0) {
                    number = number / devider;
                    sb.append(devider);
                    sb.append("*");
                }
                devider++;
            }
        }
        String s = sb.substring(0, sb.length() - 1);
        System.out.println(s);
    }

    /*.   Создайте метод, который принимает число и печатает все простые
             делители этого числа (разложение на множители)
        */
    private static int nod(int firstNumber, int secondnumber) {

        if(firstNumber==secondnumber){
            return firstNumber;
      }
        if(firstNumber>secondnumber){
            int tmp = firstNumber;
            firstNumber = secondnumber;
            secondnumber = tmp;
        }
        return nod(firstNumber,secondnumber - firstNumber);
    }
    private static int nok(int firstNumber, int secondnumber){
        // сперва находим наиб общий делитель
        int nod = nod(firstNumber,secondnumber);
        // затем при помощи НОД по известной формуле вычисляем НОК
        return Math.abs(firstNumber*secondnumber)/nod;
    }
}