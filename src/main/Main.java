package main;


import tasks.*;

import java.util.Scanner;

/**
 * Программа должна запрашивать у пользователя номер задачи и выполнять ее.
 * Программа должна уведомлять пользователя результатах выполнения, указывая назначение вывода.
 * Запрашиваемые значения должны предваряться выводом информации о назначении вводимых значений и,
 * если нужно, допустимых диапазонах.
 * Код должен быть отформатирован, содержать необходимые комментарии, следовать конвенциям по названиям
 */
public class Main {
    private static Scanner s = new Scanner(System.in);

    Main() {

        for (; ; ) {
            Menu.showMyMenu();// в этом методе нам показывается меню выбора номера задачи
            int userAnswer = s.nextInt();
            switch (userAnswer) {
                case 1:
                    TaskOne.show();
                    break;
                case 2:
                    TaskTwo.showEmptyRectangle();
                    break;
                case 3:
                    TaskTree taskTree = new TaskTree();
                case 4:
                    Treangle treangle = new Treangle();
                    break;
                case 5:
                    Treangle.firTree();
                case 6:
                    Numerals numerals = new Numerals();
                case 7:

                case 0:
                    return;
            }

        }
    }

    public static void main(String[] args) {

        new Main();
        s.close();
    }
}
