package main;



/**
 * Created by Александр on 25.12.2017.
 */
public class Menu {


    public static void showMyMenu() {
        System.out.println("Перед вами ряд задач, какую именно вы хотите выполнить??");
        StringBuilder sb = new StringBuilder();
        sb.append("1. Нарисовать прямоугольник.\n");
        sb.append("2. Нарисовать полый прямоугольник.\n");
        sb.append("3. Взять заданное число и посчитать: \n");
        sb.append("\t 1)Количесто разрядов в числе \n");
        sb.append("\t 2)Cумму разрядов этого числа \n");
        sb.append("\t 3)Наибольшую цифру в разрядах \n");
        sb.append("\t 4)Число, в котором цифры разрядов будут зеркально переставлены\n");
        sb.append("4. Нарисовать треугольник\n");
        sb.append("5. Нарисовать елочку\n");
        sb.append("6. Работа с цифрами");
        sb.append("7. Работа с одномерными массивами");
        System.out.println(sb);
    }
}
